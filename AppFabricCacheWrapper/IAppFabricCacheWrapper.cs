﻿using System;
using System.Runtime.InteropServices;

namespace Cloudbase.Cache
{
    public enum ExpirationType
    {
        Permanent = 1,
        Temporary = 2,
        TimeStamp = 3
    }

    [Guid("a8e1d1fa-b7c4-485e-9502-705860655a72")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    interface IAppFabricCacheWrapper
    {
        void Clear(string region, string keySubString);
        CacheItem GetValue(string region, string key);
        void SetCacheParameters(string host, int port, string cacheName);
        void SetValue(string region, string key, string value, ExpirationType expirationType, long epochTimeStamp, string headers);
        void Remove(string region, string key);
        void Close();
    }
}
