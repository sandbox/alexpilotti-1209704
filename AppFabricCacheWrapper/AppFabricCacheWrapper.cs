﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.ApplicationServer.Caching;
using System.Runtime.InteropServices;

namespace Cloudbase.Cache
{
    [Guid("6c45fbd0-3dc2-4f25-8e4e-06dc2f7c1334")]
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    public class CacheItem
    {
        public string DataSerialized { get; set; }
        public long Created { get; set; }
        public string Headers { get; set; }
        public long Expire { get; set; }
        public ExpirationType ExpirationType { get; set;  } 
    }


    [Guid("22661dd7-eea3-4431-a5bd-3adfcc6a4ef0")]
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    [ProgId("Cloudbase.AppFabricCacheWrapper")]
    public class AppFabricCacheWrapper : IDisposable, IAppFabricCacheWrapper
    {
        DataCacheFactory factory;
        DataCache cache;
        string host = "localhost"; 
        int port = 22233;
        string cacheName = "default";
        IList<string> regions = new List<string>();

        DataCache GetCache(string region)
        {
            lock (this)
            {
                if (cache == null)
                {
                    List<DataCacheServerEndpoint> servers = new List<DataCacheServerEndpoint>(1);

                    servers.Add(new DataCacheServerEndpoint(host, port));

                    DataCacheFactoryConfiguration configuration = new DataCacheFactoryConfiguration();
                    configuration.Servers = servers;

                    configuration.LocalCacheProperties = new DataCacheLocalCacheProperties();

                    DataCacheClientLogManager.ChangeLogLevel(System.Diagnostics.TraceLevel.Off);

                    factory = new DataCacheFactory(configuration);
                    cache = factory.GetCache(cacheName);
                }

                if (region != null)
                {
                    cache.CreateRegion(region);

                    if (!regions.Contains(region))
                        regions.Add(region);
                }

                return cache;
            }
        }

        public void SetCacheParameters(string host, int port, string cacheName)
        {
            lock (this)
            {
                this.host = host;
                this.port = port;
                this.cacheName = cacheName;

                Close();
            }
        }

        public void SetValue(string region, string key, string value, ExpirationType expirationType, long epochTimeStamp, string  headers)
        {
            CacheItem item = new CacheItem() { Created = GetEpochTimeStampFromDateTime(DateTime.Now), 
                                               DataSerialized = value, 
                                               ExpirationType = expirationType, 
                                               Expire = epochTimeStamp, 
                                               Headers = headers };

            switch (expirationType)
            {
                case ExpirationType.Permanent:
                case ExpirationType.Temporary:
                    GetCache(region).Put(key, item, new DataCacheTag [] { new DataCacheTag(expirationType.ToString()) }, region);
                    break;
                case ExpirationType.TimeStamp:
                    TimeSpan ts = GetDateTimeFromEpochTimeStamp(epochTimeStamp) - DateTime.Now;
                    GetCache(region).Put(key, item, ts, region);
                    break;
            }
        }

        private static DateTime GetDateTimeFromEpochTimeStamp(long epochTimeStamp)
        {
            return new DateTime(epochTimeStamp * 10000000L + 621355968000000000L);
        }

        private static long GetEpochTimeStampFromDateTime(DateTime dt)
        {
            return (dt.Ticks - 621355968000000000L) / 10000000L;
        }

        public CacheItem GetValue(string region, string key)
        {
            return (CacheItem)GetCache(region).Get(key, region);
        }

        public void Remove(string region, string key)
        {
            GetCache(region).Remove(key, region);
        }

        public void Clear(string region, string keySubString)
        {
            DataCache dc = GetCache(region);

            if (string.IsNullOrEmpty(keySubString))
            {
                if (region != null)
                    dc.ClearRegion(region);
                else
                    RemoveAllRegions(dc);
            }
            else if (keySubString == "*")
                RemoveRegion(region, dc);
            else
            {
                string keySubStringUC = keySubString.ToUpper();
                foreach (var kvp in dc.GetObjectsByTag(new DataCacheTag(ExpirationType.Temporary.ToString()), region))
                {
                    if (string.IsNullOrEmpty(keySubString) || kvp.Key.ToUpper().Contains(keySubStringUC))
                        dc.Remove(kvp.Key, region);
                }
            }
        }

        private void RemoveRegion(string region, DataCache dc)
        {
            lock (this)
            {
                dc.RemoveRegion(region);
                regions.Remove(region);
            }
        }

        private void RemoveAllRegions(DataCache dc)
        {
            lock (this)
            {
                foreach (var r in regions)
                    dc.RemoveRegion(r);
                regions.Clear();
            }
        }

        public void  Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~AppFabricCacheWrapper() 
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                Close();
        }

        public void Close()
        {
            lock (this)
            {
                if (factory != null)
                {
                    factory.Dispose();
                    factory = null;
                }

                cache = null;
            }
        }
    }
}
