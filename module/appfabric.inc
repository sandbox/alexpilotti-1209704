<?php

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

	  
function get_appfabric() {	
	// TODO: Cache an instance of the object to avoid some overhead
	// TODO: Using DOTNET doesn't work properly. Check why. 
	$appfabric = new COM('Cloudbase.AppFabricCacheWrapper') or die('Unable to instantiate AppFabricCacheWrapper');
	//$appfabric = new DOTNET('AppFabricCacheWrapper, Version=1.0.0.0, Culture=neutral, PublicKeyToken=b87e5601b8d8d65d', 
	//						  'Cloudbase.Cache.AppFabricCacheWrapper') or die('Unable to instantiate AppFabricCacheWrapper');
	appfabric_set_cache_parameters($appfabric);

	return $appfabric;
}


function appfabric_set_cache_parameters($appfabric) {
	$appfabric->SetCacheParameters(variable_get('appfabric_host', 'localhost'), 
								   variable_get('appfabric_port', 22233), 
								   variable_get('appfabric_cachename', 'default'));
}

							   
function cache_set($cid, $data, $table = 'cache', $expire = CACHE_PERMANENT, $headers = NULL) {
	$appfabric = get_appfabric();

	$expirationType = 3;
	if($expire == CACHE_PERMANENT)
		$expirationType = 1;
	else if($expire == CACHE_TEMPORARY)
		$expirationType = 2;
		
	$appfabric->SetValue($table, $cid, serialize($data), $expirationType, $expire, $headers);

	$appfabric->Close();
}

function cache_get($cid, $table = 'cache') {
	$appfabric = get_appfabric();
	
	$item = $appfabric->getValue($table, $cid);
	if(is_null($item))
		return 0;
	else {
		$item2 = new StdClass;
		$item2->created = $item->created;
		$item2->headers = $item->headers;
		$item2->expire = $item->expire;		
		$item2->data = unserialize($item->dataserialized);
		
		return $item2;
	}
	
	$appfabric->Close();
}

function cache_clear_all($cid = NULL, $table = NULL, $wildcard = FALSE) {
	$appfabric = get_appfabric();
	
	if(!$wildcard && $cid != NULL)
		$appfabric->Remove($table, $cid);
	else
		$appfabric->Clear($table, $cid);
		
	$appfabric->Close();	
}

?>